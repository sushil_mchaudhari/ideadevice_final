from __future__ import print_function
import os
import re  # 're' module for regular expression
import subprocess   # To run linux commands.
import crypt
import fileinput

# 'request' provides access to the incoming request data from the form
# 'render_template' renders the template with given parameters

from flask import Flask, session, request, render_template

app = Flask(__name__)  #Creation of flask app object

app.secret_key = os.urandom(24)         # Random key from session.

def is_user_name_valid(username):
    length = len(username)
    nameok = 'False';
    if re.match("^[a-z_][a-z0-9_-]*[$]?$",username):
       if ((length >= 1) & (length <= 31)):
           nameok = 'True'
    return nameok

def is_password_valid(password):
    if not re.match("((?=.*\d)(?=.*[a-z])(?=.*[@#$%]).{6,20})",password):
        return False
    else:
        return True

def is_homedir_valid(homedir):
    if not re.match("^(/)[-_A-Za-z0-9]+(/[-_A-Za-z0-9]*)*",homedir):
        return False
    else:
        return True

def is_user_present(username):
    if username == 'root':
	return False
    cmd = subprocess.Popen(["id", username],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    cmd_status = cmd.wait()
    if cmd_status:
	return False
    else:
	return True
	

def create_user(username,password,homedir,shelltype,sudop):
    encpass = crypt.crypt(password,"22")    
    if homedir:
	out = os.system("/usr/sbin/adduser -p " +encpass+ " -d " +homedir+ " -s " +shelltype+ " " +username)
    else:
	out = os.system("/usr/sbin/adduser -p " +encpass+ " -s " +shelltype+ " " +username)
    
    if sudop == 'yes':
	sudoline = username+" ALL=(ALL)       ALL"
   	with open('/etc/sudoers','a+') as f: f.write(sudoline+"\n")
    return out	
	 

def modify_user(username,password,homedir,shelltype,sudop):
    encpass = crypt.crypt(password,"22") 	
    if password and homedir:
	out = os.system("/usr/sbin/usermod -p " +encpass+ " -d " +homedir+ " -s " +shelltype+ " " +username) 
    elif not password and homedir:
	out = os.system("/usr/sbin/usermod -d " +homedir+ " -s " +shelltype+ " " +username)
    elif password and not homedir:
	out = os.system("/usr/sbin/usermod -p " +encpass+ " -s " +shelltype+ " " +username)
    else:
	out = os.system("/usr/sbin/usermod -s " +shelltype+ " " +username) 

    if sudop == 'yes':
	count = 0
	with open("/etc/sudoers") as f:
    	     for line in f:
        	if re.match('\\b'+username+'\\b',line,flags=re.IGNORECASE):
             		count = count + 1
	if count == 0:
	   sudoline = username+" ALL=(ALL)       ALL"
           with open('/etc/sudoers','a+') as f: f.write(sudoline+"\n")
    return out

def delete_user(username):
    count = 0
    with open("/etc/sudoers") as f:
         for line in f:
             if re.match('\\b'+username+'\\b',line,flags=re.IGNORECASE):
                  count = count + 1
    if count != 0:
	for line in fileinput.input("/etc/sudoers", inplace=True):
	    if re.match('\\b'+username+'\\b',line,flags=re.IGNORECASE):
        	continue
    	    print(line, end='')

    return os.system("/usr/sbin/userdel -r " +username)


@app.route('/', methods=['GET','POST'])    # view responds to the URL / for the methods GET and POST
def index():
    errors = ''
    if request.method == "GET": # If the request is GET, show the form template.
        return render_template("index2.html", errors=errors)
    else:                       # If the request is POST , get POST data and validate it.
        action = request.form.get('action', None)
        username = request.form['name'].strip()
        password = request.form['password'].strip()
        homedir = request.form['home'].strip()
        shelltype = request.form['type'].strip()
        sudop = request.form.get('sudo', None)

        ## Check for manadatory fields non-empty and if empty raise an error.

	if action:
	   if action == 'create':
       		if not username or not password:
               		errors = "Create : Username or password requird."
   	   elif action == 'modify':
       		if not username:
               		errors = "Modify : user name is mandatory."
#           	elif not password and not homedir:
#               		errors = "Modify : at least 1 filed is necessary."
   	   elif action == 'delete':
       		if not username:
               		errors = "Delete : User name is mandatory."
	else:
   	  errors = "Select action first"

	## Validate Username , Password , Home Directory.
	if username == 'root':
		errors = "root user is not allowed"
		return render_template("index2.html", errors=errors)

        if not errors:
	   if action == 'create':
                if is_user_present(username):
                          errors = "User is present. Please check username"
       	      	if not is_user_name_valid(username):
	        	  errors = "Please enter a vaild username."
                if not is_password_valid(password):
			  errors = "Please enter a vaild Password." 
	        if homedir:
		     if not is_homedir_valid(homedir):
		  	errors = "Please enter a vaild HomeDir name."

	   if action == 'modify':
	        if not is_user_present(username):
			  errors = "User is not present. Please check username"
                if password:
		    if not is_password_valid(password):
                  	errors = "Please enter a vaild Password."
                if homedir:
                    if not is_homedir_valid(homedir):
                  	errors = "Please enter a vaild HomeDir name."

	   if action == 'delete':
                if not is_user_present(username):
                	  errors = "User is not present. Please check username."

	## Create / Modify / Delete Operation.

	if not errors:
	   if action == 'create':
		if create_user(username,password,homedir,shelltype,sudop):
			errors = "Unable to create user."
	   if action == 'modify':
		if modify_user(username,password,homedir,shelltype,sudop):
			errors = "Unable to modify user."	
	   if action == 'delete':
		if delete_user(username):
			errors = "Error while deleting user"
 
        if not errors:
            data = {'action' : action,                  # Form Data.
                    'homedir' : homedir,
                    'shelltype' : shelltype,
                    'username' : username,
                    'password' : password,
                    'sudop' : sudop
                    }
            return render_template("success.html", data=data)    # If form data is valid, show the success page
        return render_template("index2.html", errors=errors)     # If form data is invalid, show error messages

if __name__ == '__main__':
  # standard port for HTTP
        app.debug = True
        app.run(
        host="0.0.0.0",
        port=int("80")
  )
 
